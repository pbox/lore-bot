## Hello

To get this to work you need to do the following

* Install ruby (> 2.1.0)
* Run `gem install bundler` to install bundler (this is ruby's package management system)
* Run `bundle install` to install all the dependencies
* Copy `.env.example` to `.env` and add your application ID and token
* Run `ruby app.rb`

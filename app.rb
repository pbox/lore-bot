require 'discordrb'
require 'dotenv'
require_relative './ishtar'

Dotenv.load # this loads secret keys from the .env file
bot = Discordrb::Commands::CommandBot.new token: ENV['TOKEN'], application_id: ENV['APPLICATION_ID'], prefix: '/'

# Here we output the invite URL to the console so the bot account can be invited to the channel. This only has to be
# done once, afterwards, you can remove this part if you want
puts "This bot's invite URL is #{bot.invite_url}."
puts 'Click on it to invite it to your server.'

bot.command :card do |event, *args|
  string = args.join(' ')
  puts "#{event.user.name} searched for #{string}"
  json = Ishtar.search_for(string)
  # I'm just reusing the slack API because it's deployed already.
  # We can make this a million times cleaner (for ruby or JS clients)
  json['attachments'][0]['fallback'] if json
end

bot.run

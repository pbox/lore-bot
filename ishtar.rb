require "json"
require "net/http"
require "uri"

module Ishtar
  def self.search_for(string)
    response = fetch(string)
    return unless response.code == '200'
    JSON.parse(response.body)
  end

  def self.fetch(string)
    string = URI.encode(string)
    uri = URI.parse("http://www.ishtar-collective.net/slack/cards/#{string}")
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    http.request(request)
  end
end
